<?php

namespace App\Http\Controllers\Backend\Service;

use App\Http\Controllers\BaseController;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('admin_pages.service.orders                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     ');
    }

    public function getAllOrders(Request $request)
    {
        //dd($request);
        if ($request->sort_by == 'is_delivered') {
            $get = Order::where('orders.is_delivered', '=', 1)
                ->join('products', 'products.id', '=', 'orders.product_id')
                ->join('users', 'users.id', '=', 'orders.user_id')
                ->select('products.name', 'products.description', 'products.price', 'products.image', 'orders.quantity', 'orders.is_delivered', 'orders.created_at', 'orders.id', 'users.name as user_name', 'users.state', 'users.address', 'users.phone')
                ->orderBy('orders.id', 'desc')
                ->paginate(10);
        } elseif ($request->sort_by == 'not_delivered') {
            $get = Order::where('orders.is_delivered', '=', 0)
                ->join('products', 'products.id', '=', 'orders.product_id')
                ->join('users', 'users.id', '=', 'orders.user_id')
                ->select('products.name', 'products.description', 'products.price', 'products.image', 'orders.quantity', 'orders.is_delivered', 'orders.created_at', 'orders.id', 'users.name as user_name', 'users.state', 'users.address', 'users.phone')
                ->orderBy('orders.id', 'desc')
                ->paginate(10);
        } else {
            if ($request->keywords !== '') {
                $key = $request->keywords;
                $searchBy = $request->search_by;
                if ($searchBy = 'user_name') {
                    $get = Order::join('products', 'products.id', '=', 'orders.product_id')
                        ->join('users', 'users.id', '=', 'orders.user_id')
                        ->select('products.name', 'products.description', 'products.price', 'products.image', 'orders.quantity', 'orders.is_delivered', 'orders.created_at', 'orders.id', 'users.name as user_name', 'users.state', 'users.address', 'users.phone')
                        ->where("users.name", 'like', '%' . $key . '%')
                        ->orderBy('orders.id', 'desc')
                        ->paginate(10);
                }
                if ($searchBy = 'product_name') {
                    $get = Order::join('products', 'products.id', '=', 'orders.product_id')
                        ->join('users', 'users.id', '=', 'orders.user_id')
                        ->select('products.name', 'products.description', 'products.price', 'products.image', 'orders.quantity', 'orders.is_delivered', 'orders.created_at', 'orders.id', 'users.name as user_name', 'users.state', 'users.address', 'users.phone')
                        ->where("products.name", 'like', '%' . $key . '%')
                        ->orderBy('orders.id', 'desc')
                        ->paginate(10);

                }
            } else {
                $get = Order::join('products', 'products.id', '=', 'orders.product_id')
                    ->join('users', 'users.id', '=', 'orders.user_id')
                    ->select('products.name', 'products.description', 'products.price', 'products.image', 'orders.quantity', 'orders.is_delivered', 'orders.created_at', 'orders.id', 'users.name as user_name', 'users.state', 'users.address')
                    ->orderBy('orders.id', 'desc')
                    ->paginate(10);

            }

        }
        return json_encode($this->reportSuccess('Data retrived successfully', $get));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function setSeenOrders($id)
    {
        $update = Order::find($id);
        $update->is_delivered = 1;
        $update->update();
        return json_encode($this->reportSuccess('Orders Updated successfully'));
    }

    public function destroy($id)
    {
        $delete = Order::where('id', $id)->first();
        $delete->delete();
        if ($delete) {
            return json_encode($this->reportSuccess('Orders deleted successfully'));
        } else {
            return json_encode($this->reportError('failed to delete'));
        }

    }
}