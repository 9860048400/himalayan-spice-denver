<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\BaseController;
use App\Models\Asset;
use App\Models\Blog;
use App\Models\Category;
use App\Models\Contact;
use App\Models\Info;
use App\Models\Product;
use App\Models\Reservation;
use App\Models\Script;
use App\Models\Slider;
use App\Models\User;
use Illuminate\Http\Request;

class FrontendController extends BaseController
{
    // home page --------------------------------------------
    public function home()
    {
        $slider = Slider::all();
        $map = Asset::take(1)->where('type', 'map')->get();
        $menu_image = Asset::take(1)->where('type', 'menu_image')->first();
        $menu_file = Asset::take(1)->where('type', 'menu_file')->first();
        $blogs = Blog::take(8)->orderBy('id', 'desc')->get();

        $category = Category::take(6)->get();
        $script = Script::all();

        $blog = Blog::orderBy('id', 'desc')->take(4)->get();

        $product = Product::all();

        // dd($services->all());

        return view('frontend_pages.index', [
            'slider' => $slider,
            'map' => $map,
            'blogs' => $blogs,
            'script' => $script,
            'product' => $product,
            'category' => $category,
            'blog' => $blog,
            'menu_image' => $menu_image,
            'menu_file' => $menu_file,
        ]);
    }

    public function reservation()
    {
        return view('frontend_pages.services.reservation', [

        ]);

    }

    public function Menu()
    {
        $category = Category::take(6)->get();

        return view('frontend_pages.menu.menu', [
            'category' => $category,
        ]);

    }

    public function MenuCategoryView($slug)
    {
        $category = Category::take(6)->get();
        $cat_id = Category::where('slug', $slug)->first();
        return view('frontend_pages.menu.menu-category', [
            'slug' => $slug,
            'cat_id' => $cat_id->id,
            'category' => $category,
        ]);

    }

    // blog details page --------------------------------
    public function BlogDetail($slug)
    {
        $b = Blog::where('slug', $slug)->take(1)->get();
        $related = Blog::where('category', $b[0]->category)->take(4)->get();
        $recent = Blog::take(6)->orderBy('id', 'desc')->get();

        $blog = $b[0];

        return view('frontend_pages.blog.blog-details', [
            'blog' => $blog,
            'related' => $related,
            'recent' => $recent,
        ]);
    }

    // blog category page
    public function blogCategory($category)
    {

        $blog = Blog::where('category', $category)->where('featured', 1)
            ->orderBy('id', 'desc')->take(3)->get();
        // dd($main_category);
        return view('frontend_pages.blog.blog-category', [
            'blog' => $blog,
            'category' => $category,
        ]);
    }

    // blog category page
    public function getBlogByCategory(Request $request)
    {
        $last_id = $request->last_id;

        if ($request->last_id == 0) {
            $last = Blog::orderBy('id', 'desc')->first();
            $last_id = $last->id;
        }

        // dd($request);
        $blog = Blog::where('category', $request->category)
            ->where('id', '<', $last_id)
            ->orderBy('id', 'desc')
            ->take(6)->get();
        // dd($blog);
        return json_encode($this->reportSuccess('Data retrived Successfully', $blog));
    }

    public function Blog()
    {
        $blog = Blog::orderBy('id', 'desc')->get()->groupBy('category');
        return view('frontend_pages.blog.blog', [
            'blog' => $blog,
        ]);
    }

    public function about()
    {
        return view('frontend_pages.about', [

        ]);
    }

    // info pages like - about us , terms and conditions-------------------
    public function infos($slug)
    {
        $i = Info::where('slug', $slug)->take(1)->get();
        if (count($i) != 0) {
            $infos = $i[0];
            return view('frontend_pages.infos', [
                'infos' => $infos,

            ]);
        } else {
            return redirect('/');
        }

    }

    // get logged in user details
    public function getLoggedUser()
    {
        if (auth()->check()) {
            $user = User::where('id', auth()->user()->id)
                ->select('email', 'phone', 'name')
                ->first();
            return json_encode($this->reportSuccess('Seat Booked Successfully. We will contact you soon. Thank you.', $user));
        }
    }

    // saving contact details --------------------------
    public function saveReservation(Request $request)
    {
        // dd($request->all());
        $validate = $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email',
            'phone' => 'required|string',
            'date' => 'required|string',
            'time' => 'required|string',
            'message' => 'required|string',
            'persons' => 'required|integer',
        ]);

        $store = new Reservation();
        $store->name = $request->name;
        $store->phone = $request->phone;
        $store->email = $request->email;
        $store->date = $request->date;
        $store->time = $request->time;
        $store->persons = $request->persons;
        $store->message = $request->message;
        $store->save();
        if ($store) {
            return json_encode($this->reportSuccess('Seat Booked Successfully. We will contact you soon. Thank you.'));

        } else {
            return json_encode($this->reportError('Failed to Book Seat. Please try again'));
        }

    }

    // contact page
    public function contact()
    {
        $map = Asset::where('type', 'map')->first();
        return view('frontend_pages.contact', [
            'map' => $map,
        ]);
    }

    // saving contact details --------------------------
    public function saveContact(Request $request)
    {
        // dd($request->all());
        $validate = $request->validate([
            'name' => 'required|string',
            'email' => 'required|string',
            'phone' => 'required|string',
            'message' => 'required|string',
        ]);

        $store = new Contact();
        $store->name = $request->name;
        $store->phone = $request->phone;
        $store->email = $request->email;
        $store->message = $request->message;
        $store->save();
        if ($store) {
            return json_encode($this->reportSuccess('Message sent Successfully. Our team will contact you soon.'));

        } else {
            return json_encode($this->reportError('Message send failed. Please try again'));
        }

    }

}