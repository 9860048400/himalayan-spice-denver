<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_roles')->insert([
            'name' => 'superadmin',
            'label' => 'Super Admin',
        ], [
            'name' => 'admin',
            'label' => 'Admin',
        ], [
            'name' => 'editor',
            'label' => 'Editor',
        ],
        );

        DB::table('users')->insert([
            'name' => 'Himalayan Spice Denver',
            'email' => 'admin@himalayan.com',
            'password' => Hash::make('SpicyLLC@co2021'),
            'is_admin' => true,
            'role_id' => 1,

        ]);

    }
}