<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->nullable();

            $table->unsignedInteger('cat_id')->nullable();
            $table->foreign('cat_id')->references('id')->on('categories');

            $table->boolean('status')->default(0)->nullable();
            $table->boolean('featured')->default(0)->nullable();
            $table->string('description')->nullable();
            $table->unsignedInteger('units')->default(0)->nullable();
            $table->double('price');
            $table->string('image');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}