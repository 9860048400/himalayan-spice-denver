require("../bootstrap");

window.Vue = require("vue").default;

Vue.component("logo", require("../components/logo.vue").default);
Vue.component("loading", require("../components/loading.vue").default);
Vue.component("login-form", require("./login/login").default);

Vue.component("blog-category", require("./blog/blog-category.vue").default);

Vue.component("reservation", require("./services/reservation").default);
Vue.component("products", require("./products/products").default);
Vue.component("latest-recipie", require("./products/latest-recipie").default);
Vue.component("cart", require("./cart/cart").default);
Vue.component("navcart", require("./cart/nav-cart").default);

const app = new Vue({
    el: "#app"
});
