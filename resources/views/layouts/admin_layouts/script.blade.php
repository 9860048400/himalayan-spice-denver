<script type="application/javascript">
    // alert notification remove
    setTimeout(() => {
        var x = $('.alert').css('display','none');
    }, 5000);
</script>


<!-- Argon Scripts -->
  <!-- Core -->
  <script src="{{asset('/assets/admin/vendor/js-cookie/js.cookie.js')}}" defer></script>
  <script src="{{asset('/assets/admin/vendor/jquery.scrollbar/jquery.scrollbar.min.js')}}" defer></script>
  <script src="{{asset('/assets/admin/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js')}}" defer></script>

  <!-- Argon JS -->
  <script src="{{asset('/assets/admin/js/argon.js?v=1.2.0')}}" defer></script>
</body>
</html>