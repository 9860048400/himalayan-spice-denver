 <!-- Footer -->
<footer class="footer pt-0 mt-6 p-3">
    <div class="row align-items-center justify-content-lg-between">
        <div class="col-12">
        <div class="copyright text-center  text-lg-center  text-muted">
            &copy; 2020 Himalayan Spicy LLC
        </div>
        </div>  
    </div>
</footer>